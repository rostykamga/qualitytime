/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.security;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.ArrayList;

/**
 *
 * @author Rostand
 */
public class KeyStore {
    
    private static boolean loaded=false;
    private static final ArrayList<String> keys= new ArrayList();
    
    private static void loadKeys(InputStream inputStream) throws IOException, URISyntaxException{
        
        //File f= new File(filename);
        try (InputStreamReader fileReader = new InputStreamReader(inputStream)) {
            BufferedReader bufferedReader= new BufferedReader(fileReader);
            String line =bufferedReader.readLine();
            
            while(line!=null)
            {
                line= line.trim();
                if(!line.isEmpty())
                    keys.add(line);
                line =bufferedReader.readLine();
            }
        }
    }
    
    public static boolean isValidKey(String key) throws IOException, URISyntaxException{
        
        if(!loaded){
            //getClass().getResource("/app/resources/btnOff.png"))S
            InputStream u1= KeyStore.class.getResourceAsStream("/app/resources/eulacodes.txt");
            InputStream u2= KeyStore.class.getResourceAsStream("/app/resources/eulacodes2.txt");
            InputStream u3= KeyStore.class.getResourceAsStream("/app/resources/items.txt");
            //File f= new
            loadKeys(u1);
            loadKeys(u2);
            loadKeys(u3);
            loaded=true;
        }
        
        return keys.contains(key.trim());
    }
}
