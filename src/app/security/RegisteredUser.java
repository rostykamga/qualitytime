/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.security;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Rostand
 */
public class RegisteredUser implements Serializable{
    
    private final String username;
    private final String licenseKey;
    private final Date registrationDate;

    public RegisteredUser(String username, String licenseKey) {
        this.username = username;
        this.licenseKey = licenseKey;
        registrationDate= new Date();
    }

    public String getUsername() {
        return username;
    }

    public String getLicenseKey() {
        return licenseKey;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }
    
    
}
