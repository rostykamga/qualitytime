/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.main;
import app.gui.MainWindow;
import app.gui.SerialKeyDialog;
import app.security.KeyStore;
import app.security.RegisteredUser;
import java.awt.AWTException;
import java.awt.Toolkit;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 *
 * @author Rostand
 */
public class Main {

    public static RegisteredUser user;
    public static final String LICENSEFILE= "./lib/native_api.dll";
    
    /**
     * @param args the command line arguments
     * @throws java.awt.AWTException
     * @throws java.io.IOException
     */
    public static void main(String args[]) throws AWTException, IOException {
        
        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
            
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        if(!existsRegisteredUser()){
            
            SerialKeyDialog dialog= new SerialKeyDialog(null, true);
            dialog.setLocationRelativeTo(null);
            dialog.setVisible(true);
            //com.fasterxml.jackson.annotation.JsonAutoDetect;
        }
        
        final MainWindow win= new MainWindow();
        win.setIconImage(Toolkit.getDefaultToolkit().getImage(Main.class.getResource("/app/resources/icon.png")));
        win.setLocationRelativeTo(null);
        
        //win.setResizable(false);
        //win.pack();
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                win.setVisible(true);
            }
        });
    }
    
    private static boolean existsRegisteredUser(){
        
        try {
            FileInputStream fis = new FileInputStream(LICENSEFILE);
            ObjectInputStream ois = new ObjectInputStream(fis);
            user = (RegisteredUser)ois.readObject();
            
            return KeyStore.isValidKey(user.getLicenseKey());
        }
        catch(Exception e){
            return false;
        }
    }
}
