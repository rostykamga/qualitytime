///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package app.core;
//
//
//import app.config.Settings;
//import com.dropbox.core.DbxClient;
//import com.dropbox.core.DbxException;
//import com.dropbox.core.DbxRequestConfig;
//import com.dropbox.core.DbxWriteMode;
//import com.dropbox.core.json.JsonReader.FileLoadException;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.util.Locale;
//
///**
// * A simple utility class for dropbox uploading. 
// * Use the {@link #DbxUploader(String, String) Auth file constructor} for the first time,
// * and the {@link #DbxUploader(String) Auth token file constructor} for any subsequent calls
// *
// */
//public class DbxUploader {
//
//    private final DbxClient _dbxClient;
//    private static DbxUploader uploader;
//
//    
//    public static synchronized void uploadFile(String filename)throws Exception{
//        
//        if(uploader==null){
//            uploader= new DbxUploader();
//        }
//        
//        String access_token=Settings.getProperty("app.dropbox.access_token");
//        if(!access_token.equals(uploader._dbxClient.getAccessToken()))
//            uploader= new DbxUploader();
//        
//        File f= new File(filename);
//        
//        uploader.upload(filename, "/Screenshot_app/videos/"+f.getName());
//    }
//
//    /**
//     * 
//     * @throws FileLoadException
//     * @throws IOException
//     */
//    public DbxUploader() throws FileLoadException, IOException {
//
//        String access_token=Settings.getProperty("app.dropbox.access_token");
//
//        // Create a DbxClient, which is what you use to make API calls.
//        String userLocale = Locale.getDefault().toString();
//        DbxRequestConfig requestConfig = new DbxRequestConfig("DbxUploader", userLocale);
//        
//        _dbxClient = new DbxClient(requestConfig, access_token);
//    }
//
//    /**
//     * Upload a file to your Dropbox
//     * @param srcFilename path to the source file to be uploaded e.g. /tmp/upload.txt
//     * @param destFilename path to the destination. Must start with '/' and must NOT end with '/'
//     *  e.g. /target_dir/upload.txt 
//     * @throws IOException 
//     * @throws com.dropbox.core.DbxException 
//     */
//    public void upload (String srcFilename, String destFilename) throws IOException, DbxException {
//
//        File uploadFile = new File (srcFilename);
//        FileInputStream uploadFIS = new FileInputStream(uploadFile);
//
//        try {
//                _dbxClient.uploadFile(destFilename, DbxWriteMode.add(), uploadFile.length(), uploadFIS);
//            } 
//        catch (DbxException e) {
//            uploadFIS.close();
//            throw e;
//        }
//    }
//	
//}