/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.core;

import app.config.Settings;
import static app.core.ScreenCapturer.convertToType;
import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.xuggler.IRational;
import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.Observable;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.JOptionPane;
import javax.swing.Timer;

/**
 *
 * @author Rostand
 */
public class ScreenCapturer extends Observable implements ActionListener{
    
    public static final int DELAY=12000; // sleep 12 seconds after each screen shot
    private static  IRational FRAME_RATE= IRational.make(1000,1000);
    private final AtomicInteger counter=new AtomicInteger(0);
    private String currentSessionFilename;
    private IMediaWriter writer;
    private String path;
    private final Robot robot;
    private final Rectangle screen;
    private long debut;
    private Timer timer;
    //private final ScreenCaptureTask screenCaptureTask;
    private  DbxUploaderV2 dropbox;
    private File accessKeyFile;
    private String authFileContent;
    private static final String template= "{\n" +
                                    "  \"key\": \"#KEY#\",\n" +
                                    "  \"secret\": \"#SECRET#\",\n" +
                                    "  \"access_token\": \"#TOKEN#\"\n" +
                                    "}";
    
    /*
     Creates a new screenshoot capturer, with the path where to store images. The delay is in milliseconds.
    */
    public ScreenCapturer(String _path) throws AWTException{
        
        this.path= _path;
        robot = new Robot();
        screen= new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
        
        timer= new Timer(DELAY, this);
        
        if(!path.endsWith(File.separator))
            path+=File.separator;
        
        File f= new File(path);
        if (!f.exists())
            f.mkdirs();
    }
    
    public void start() throws Exception{
        
        
        authFileContent= template.replace("#KEY#", Settings.getProperty("app.dropbox.api_key"));
        authFileContent= authFileContent.replace("#SECRET#", Settings.getProperty("app.dropbox.secret"));
        authFileContent= authFileContent.replace("#TOKEN#", Settings.getProperty("app.dropbox.access_token"));

        accessKeyFile= File.createTempFile("accesskey", null);
        FileWriter w= new FileWriter(accessKeyFile);
        w.append(authFileContent);
        w.flush();
        w.close();
        
        if(dropbox==null){
            dropbox= new DbxUploaderV2(accessKeyFile.getAbsolutePath());
        }else
            dropbox.updateInfo(accessKeyFile.getAbsolutePath());
        
        Timestamp t=new Timestamp(System.currentTimeMillis());
        String s=t.toString();
        s=s.replace(":", "_");
        currentSessionFilename="video_session"+s+".mp4";
        counter.set(0);
        
        setChanged();
        notifyObservers("Started...");
        
        timer.start();
    }
    
    public void stop(){
        
        timer.stop();
        try {
            // Finish the video generation
            if(counter.get()>0){
                setChanged();
                notifyObservers("Compiling  Video...");

                writer.close();
                writer=null;
                //writer.
                setChanged();
                notifyObservers("Uploading  Video...");

                //Upload the video to dropbox, and delete it from the local system
                dropbox.upload(currentSessionFilename);
                File f= new File(currentSessionFilename);
                //f.deleteOnExit();
                //f.delete();
                
            }
            setChanged();
            notifyObservers("Finished");
        } catch (Exception ex) {
           // ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "A fatal error occured while generating or uploading the video\n"+ex.getMessage());
            setChanged();
            notifyObservers("Finished");
        }
    }
    
    public long getNbScreenshoot(){
        
        return counter.get();
    }

    
      /**
   * Convert a {@link BufferedImage} of any type, to {@link BufferedImage} of a
   * specified type. If the source image is the same type as the target type,
   * then original image is returned, otherwise new image of the correct type is
   * created and the content of the source image is copied into the new image.
   * 
   * @param sourceImage
   *          the image to be converted
   * @param targetType
   *          the desired BufferedImage type
   * 
   * @return a BufferedImage of the specifed target type.
   * 
   * @see BufferedImage
   */

  public static BufferedImage convertToType(BufferedImage sourceImage,int targetType){
    BufferedImage image;

    // if the source image is already the target type, return the source image
    if (sourceImage.getType() == targetType)
      image = sourceImage;

    // otherwise create a new image of the target type and draw the new
    // image
    else
    {
      image = new BufferedImage(sourceImage.getWidth(),
          sourceImage.getHeight(), targetType);
      image.getGraphics().drawImage(sourceImage, 0, 0, null);
    }

    return image;
  }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        try{
            
            if(writer==null){
                // First, let's make a IMediaWriter to write the file.
                writer = ToolFactory.makeWriter(currentSessionFilename);

                // We tell it we're going to add one video stream, with id 0,
                // at position 0, and that it will have a fixed frame rate of FRAME_RATE.
                writer.addVideoStream(0, 0, FRAME_RATE, screen.width, screen.height);
            }
            // Take a screen shot
            BufferedImage screenShot = robot.createScreenCapture(screen);

            Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName("jpg");
            ImageWriter imgWriter = iter.next();
            ImageWriteParam iwp = imgWriter.getDefaultWriteParam();
            iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            iwp.setCompressionQuality(0.40f);

            File compressedImageFile = new File("./Temp/"+counter.get()+".jpg");
            ImageOutputStream ios;
            try (OutputStream os = new FileOutputStream(compressedImageFile)) {
                ios = ImageIO.createImageOutputStream(os);
                imgWriter.setOutput(ios);
                imgWriter.write(null, new IIOImage(screenShot, null, null), iwp);
            }
            ios.close();
            imgWriter.dispose();
               
            BufferedImage image= ImageIO.read(compressedImageFile);
            BufferedImage bgrScreen = convertToType(image, BufferedImage.TYPE_3BYTE_BGR);

            // convert to the right image type
            long interval= (long)(((System.nanoTime()-debut)*1000)/DELAY);

            writer.encodeVideo(0, bgrScreen, interval, TimeUnit.NANOSECONDS);
            
            compressedImageFile.delete();
            counter.incrementAndGet();
        }
        catch(Exception ex){
            
        }
    }
    
}


//class ScreenCaptureTask extends TimerTask{
//
//    private final Robot robot;
//    private final Rectangle screen;
//    private final AtomicInteger counter=new AtomicInteger(0);
//    private IMediaWriter writer;
//    private final long  period;
//    private long debut;
//    
//    
//    public ScreenCaptureTask(Robot robot, Rectangle screen, long delay){
//        this.robot= robot;
//        this.screen= screen;
//        this.period= delay;
//    }
//    
//    public long getCounter(){
//        
//        return counter.get();
//    }
//    
//    
//    public void setMediaWriter(IMediaWriter writer){
//        
//        counter.set(0);
//        debut= System.nanoTime();
//        this.writer= writer;
//    }
//    
//    
//    @Override
//    public void run() {
//        
//        
//    }
//    
//}