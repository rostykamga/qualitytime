/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.core;

import com.dropbox.core.DbxAuthInfo;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWebAuth;
import com.dropbox.core.NetworkIOException;
import com.dropbox.core.RetryException;
import com.dropbox.core.json.JsonReader;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.CommitInfo;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.UploadSessionCursor;
import com.dropbox.core.v2.files.UploadSessionFinishErrorException;
import com.dropbox.core.v2.files.UploadSessionLookupErrorException;
import com.dropbox.core.v2.files.WriteMode;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Locale;
import java.util.Observable;
//import org.apache.log4j.Level;

/**
 * An example command-line application that runs through the web-based OAuth
 * flow (using {@link DbxWebAuth}).
 */
public class DbxUploaderV2 extends Observable{
    // Adjust the chunk size based on your network speed and reliability. Larger chunk sizes will
    // result in fewer network requests, which will be faster. But if an error occurs, the entire
    // chunk will be lost and have to be re-uploaded. Use a multiple of 4MiB for your chunk size.
    private static final long CHUNKED_UPLOAD_CHUNK_SIZE = 8L << 20; // 8MiB
    private static final int CHUNKED_UPLOAD_MAX_ATTEMPTS = 5;
    private final String argAuthFile;
    private DbxAuthInfo authInfo;
    
    public DbxUploaderV2(String autthFile)throws Exception{
        
        this.argAuthFile= autthFile;
        
        authInfo = DbxAuthInfo.Reader.readFromFile(argAuthFile);
    }
    
    public void updateInfo(String autthFile) throws JsonReader.FileLoadException{
        authInfo = DbxAuthInfo.Reader.readFromFile(autthFile);
    }

    /**
     * Uploads a file in a single request. This approach is preferred for small files since it
     * eliminates unnecessary round-trips to the servers.
     *
     * @param dbxClient Dropbox user authenticated client
     * @param localFIle local file to upload
     * @param dropboxPath Where to upload the file to within Dropbox
     */
    private  void uploadFile(DbxClientV2 dbxClient, File localFile, String dropboxPath) throws Exception {

        InputStream in = new FileInputStream(localFile);
        FileMetadata metadata = dbxClient.files().uploadBuilder(dropboxPath)
            .withMode(WriteMode.ADD)
            .withClientModified(new Date(localFile.lastModified()))
            .uploadAndFinish(in);

        //System.out.println(metadata.toStringMultiline());
    }

    /**
     * Uploads a file in chunks using multiple requests. This approach is preferred for larger files
     * since it allows for more efficient processing of the file contents on the server side and
     * also allows partial uploads to be retried (e.g. network connection problem will not cause you
     * to re-upload all the bytes).
     *
     * @param dbxClient Dropbox user authenticated client
     * @param localFIle local file to upload
     * @param dropboxPath Where to upload the file to within Dropbox
     */
    private  void chunkedUploadFile(DbxClientV2 dbxClient, File localFile, String dropboxPath) throws Exception{
        
        long size = localFile.length();

        long uploaded = 0L;
        DbxException thrown = null;

        // Chunked uploads have 3 phases, each of which can accept uploaded bytes:
        //
        //    (1)  Start: initiate the upload and get an upload session ID
        //    (2) Append: upload chunks of the file to append to our session
        //    (3) Finish: commit the upload and close the session
        //
        // We track how many bytes we uploaded to determine which phase we should be in.
        String sessionId = null;
        for (int i = 0; i < CHUNKED_UPLOAD_MAX_ATTEMPTS; ++i) {
            if (i > 0) {
                setChanged();
                String msg= String.format("Retrying chunked upload (%d / %d attempts)", i + 1, CHUNKED_UPLOAD_MAX_ATTEMPTS);
                notifyObservers(msg);
            }

            try (InputStream in = new FileInputStream(localFile)) {
                // if this is a retry, make sure seek to the correct offset
                in.skip(uploaded);

                // (1) Start
                if (sessionId == null) {
                    sessionId = dbxClient.files().uploadSessionStart()
                        .uploadAndFinish(in, CHUNKED_UPLOAD_CHUNK_SIZE)
                        .getSessionId();
                    uploaded += CHUNKED_UPLOAD_CHUNK_SIZE;
                    printProgress(uploaded, size);
                }

                // (2) Append
                while ((size - uploaded) > CHUNKED_UPLOAD_CHUNK_SIZE) {
                    dbxClient.files().uploadSessionAppend(sessionId, uploaded)
                        .uploadAndFinish(in, CHUNKED_UPLOAD_CHUNK_SIZE);
                    uploaded += CHUNKED_UPLOAD_CHUNK_SIZE;
                    printProgress(uploaded, size);
                }

                // (3) Finish
                long remaining = size - uploaded;
                UploadSessionCursor cursor = new UploadSessionCursor(sessionId, uploaded);
                CommitInfo commitInfo = CommitInfo.newBuilder(dropboxPath)
                    .withMode(WriteMode.ADD)
                    .withClientModified(new Date(localFile.lastModified()))
                    .build();
                FileMetadata metadata = dbxClient.files().uploadSessionFinish(cursor, commitInfo)
                    .uploadAndFinish(in, remaining);

                setChanged();
                notifyObservers("Finished !!!");
                return;
                
            } catch (RetryException ex) {
                thrown = ex;
                // RetryExceptions are never automatically retried by the client for uploads. Must
                // catch this exception even if DbxRequestConfig.getMaxRetries() > 0.
                sleepQuietly(ex.getBackoffMillis());
                continue;
            } catch (NetworkIOException ex) {
                thrown = ex;
                // network issue with Dropbox (maybe a timeout?) try again
                continue;
            } catch (UploadSessionLookupErrorException ex) {
                if (ex.errorValue.isIncorrectOffset()) {
                    thrown = ex;
                    // server offset into the stream doesn't match our offset (uploaded). Seek to
                    // the expected offset according to the server and try again.
                    uploaded = ex.errorValue
                        .getIncorrectOffsetValue()
                        .getCorrectOffset();
                    continue;
                } else {
                    // Some other error occurred, give up.
                    setChanged();
                    notifyObservers("Error uploading to Dropbox: " + ex.getMessage());
                    setChanged();
                    notifyObservers("Stop");
                    return;
                }
            } catch (UploadSessionFinishErrorException ex) {
                if (ex.errorValue.isLookupFailed() && ex.errorValue.getLookupFailedValue().isIncorrectOffset()) {
                    thrown = ex;
                    // server offset into the stream doesn't match our offset (uploaded). Seek to
                    // the expected offset according to the server and try again.
                    uploaded = ex.errorValue
                        .getLookupFailedValue()
                        .getIncorrectOffsetValue()
                        .getCorrectOffset();
                    continue;
                } else {
                    // some other error occurred, give up.
                    setChanged();
                    notifyObservers("Error uploading to Dropbox: " + ex.getMessage());
                    setChanged();
                    notifyObservers("Stop");
                    return;
                }
            } catch (DbxException ex) {
                setChanged();
                notifyObservers("Error uploading to Dropbox: " + ex.getMessage());
                setChanged();
                notifyObservers("Stop");
                return;
            } catch (IOException ex) {
                setChanged();
                notifyObservers("Error reading from file \"" + localFile + "\": " + ex.getMessage());
                setChanged();
                notifyObservers("Stop");
                return;
            }
        }
        // if we made it here, then we must have run out of attempts
        //System.exit(1);
        setChanged();
        notifyObservers("Maxed out upload attempts to Dropbox. Most recent error: " + thrown.getMessage());
        setChanged();
        notifyObservers("Stop");
    }

    
    private  void printProgress(long uploaded, long size) {
        
        setChanged();
        String msg= String.format("Uploaded %12d / %12d bytes (%5.2f%%)", uploaded, size, 100 * (uploaded / (double) size));
        notifyObservers(msg);
    }

    private  void sleepQuietly(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ex) {
            // just exit
            setChanged();
            String msg= String.format("Error uploading to Dropbox: interrupted during backoff.");
            notifyObservers(msg);
            setChanged();
            notifyObservers("Stop");
        }
    }

    public  void upload(String localPath) throws Exception {
        // Only display important log messages.

        File localFile = new File(localPath);
        String dropboxPath= "/Screenshot_app/videos/"+localFile.getName();
        

        // Create a DbxClientV2, which is what you use to make API calls.
        String userLocale = Locale.getDefault().toString();
        DbxRequestConfig requestConfig = new DbxRequestConfig("screenCaptureApp", userLocale);
        DbxClientV2 dbxClient = new DbxClientV2(requestConfig, authInfo.getAccessToken(), authInfo.getHost());

        // upload the file with simple upload API if it is small enough, otherwise use chunked
        // upload API for better performance. Arbitrarily chose 2 times our chunk size as the
        // deciding factor. This should really depend on your network.
        if (localFile.length() <= (2 * CHUNKED_UPLOAD_CHUNK_SIZE)) {
            uploadFile(dbxClient, localFile, dropboxPath);
        } else {
            chunkedUploadFile(dbxClient, localFile, dropboxPath);
        }
    }
}
